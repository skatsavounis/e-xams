import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , of } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class ApicodeService {

    private _codeUrl = "http://anarchyologist.local/am/code/";


  constructor(
        private http:HttpClient,
        private router:Router

  ) { }
 

   onSumbit(value: string) {
        this.http.post<any>(this._codeUrl, {'temptoken' : localStorage.getItem('UserToken'), 'code' : value})
        .pipe(
            tap( res => {
                console.log(res);
                return res;
            }),
             catchError(err => {
                 console.log('kati dn paei kala', err);
                 return err;            
            }),
        ).subscribe();



   }

}
