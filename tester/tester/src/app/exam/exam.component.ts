import { Component, Input, OnInit } from '@angular/core';
import { CodeModel } from './thecode';
import { ApicodeService } from './apicode.service';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {
  
 

  constructor(private apicode: ApicodeService) { }

  ngOnInit(): void {
  }
 
  codeModel: CodeModel = {
    language: 'typescript',
    uri: 'main.ts',
    value: '',
    dependencies:  [
      '@types/node',
      '@ngstack/translate', 
      '@ngstack/code-editor'
    ]
 
  };
 
 
  options = {
    contextmenu: true,
    minimap: {
      enabled: true
    }
  };

  
  onSubmit(value) {
      console.log(this.codeModel.value);
      this.apicode.onSumbit(this.codeModel.value);
  }


}
