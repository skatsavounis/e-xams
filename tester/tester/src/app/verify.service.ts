import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , of } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class VerifyService {

    private  _loginUrl = "http://anarchyologist.local/am/login/";
    private  _checkUrl = "http://anarchyologist.local/am/check/";

    constructor(
        private http:HttpClient,
        private router:Router
    ) { }

    public isAuthenticated(): Observable<boolean> {
        const token = localStorage.getItem('UserToken');
        return this.http.post<any>(this._checkUrl, { 'token' : token })
        .pipe(
            map( res => {
                console.log(res);
                if (res == true) {
                    return true;
                } else {
                    return false;
                }}),
                catchError(err => {
                    console.log('kati dn paei kala', err);
                    return of(false);
                }),
        );
    }

    loginT(AM: string, unique_id: string) {
        this.http.post<any>(this._loginUrl, { 'AM': AM , 'unique_id' : unique_id } )
        .pipe(
            tap( res => {
                console.log(res);
                localStorage.setItem('UserToken', res);
                this.router.navigate(['/home']);
            }),
            catchError(err => {
                console.log('kati dn paei kala', err);
                return err;
            }),
        ).subscribe();
    }


}
