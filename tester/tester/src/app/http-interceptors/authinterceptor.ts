import { Injectable } from "@angular/core";
import { VerifyService } from '../verify.service'; 
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse ,  HttpErrorResponse } from '@angular/common/http';
import { NavigationEnd, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of , throwError } from 'rxjs';
import { retry , map, catchError, mergeMap, tap } from 'rxjs/operators';
import { debug } from 'util';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    
    constructor(private verify: VerifyService,
                private router: Router,
                private http: HttpClient,
                ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler ):Observable<HttpEvent<any>> {
       request  = request.clone({
       setHeaders: { Authorization: `${localStorage.getItem('UserToken')}`  }
       }) ;
        return next.handle(request);
    }
}




