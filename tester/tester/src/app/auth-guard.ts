import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { VerifyService } from './verify.service';
import { Observable , of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate
{


    constructor(
        private verify: VerifyService, 
        private router: Router
    ) {}  

    canActivate(
        next: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean>| boolean {

            var k = this.verify.isAuthenticated(); 
            k.pipe(
                tap( res => { 
                    if(!res) {
                        this.router.navigate(['/login']);
                    }
                }),
            ).subscribe();

            return k;
         /*   if (!this.verify.isAuthenticated()) {
                this.router.navigate(['/login']);
                return false;
            } else { return true; }*/

         //   return this.verify.isAuthenticated().pipe(
           //     catchError(err => {
           //         console.log(err);
           //        this.router.navigate(['login']);
            //        return of(false);
            //    })
           // );
        }
}
