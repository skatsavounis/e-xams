import { Component, OnInit } from '@angular/core';
import { VerifyService } from '../verify.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-testerlogin',
    templateUrl: './testerlogin.component.html',
    styleUrls: ['./testerlogin.component.css']
})
export class TesterloginComponent implements OnInit {


    public AM: string;
    public unique_id: string;


    constructor(private verify: VerifyService) { }

    ngOnInit(): void {
    }

    loginT() {
        this.verify.loginT(this.AM, this.unique_id);
    
    }

}


