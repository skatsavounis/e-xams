import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TesterloginComponent } from './testerlogin/testerlogin.component';
import { ShowResultComponent } from './testerlogin/show-result/show-result.component';

import {VerifyService} from './verify.service';
import {ApicodeService} from './exam/apicode.service';

import { httpInterceptProviders } from './http-interceptors';
import { AuthGuard } from './auth-guard';

import { CodeEditorModule } from '@ngstack/code-editor';

import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { ExamComponent } from './exam/exam.component';



@NgModule({
  declarations: [
    AppComponent,
    TesterloginComponent,
    ShowResultComponent,
    HomeComponent,
    ExamComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    CodeEditorModule.forRoot()
  ],
  providers: [VerifyService, AuthGuard, httpInterceptProviders,ApicodeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
