import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { TesterloginComponent } from './testerlogin/testerlogin.component';
import { AuthGuard } from './auth-guard';
import { HomeComponent } from './home/home.component';
import { ExamComponent } from './exam/exam.component';

const routes: Routes = [   

    { path: '',
      pathMatch: 'full',
      redirectTo: '/home',
     },
    { path: 'login', 
      component: TesterloginComponent,
    },

    { path: 'home',
      component: HomeComponent,
      canActivate: [AuthGuard],
    },
    { path: 'exam',
      component: ExamComponent,
      canActivate: [AuthGuard],
    },
    {path: '**', redirectTo: 'login', pathMatch: 'full'},

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
