from django.contrib import admin
from django.urls import path


from django.conf import settings
from django.conf.urls import url, include
from TestersApp.viewsets import AmViewSet
from rest_framework import routers
from rest_framework.routers import DefaultRouter
from django.conf.urls.static import static

router = routers.DefaultRouter()
router.register(r'', AmViewSet)


urlpatterns = [


    path('admin/', admin.site.urls),
  
    path('am/', include(router.urls)),
    ]
