import uuid
from django.db import models

# Create your models here.


class AM(models.Model):
    TestersAM = models.CharField(max_length=5)
    unique_id = models.CharField(max_length=100, default=uuid.uuid4, editable=False, unique=True)
    examtoken = models.CharField(max_length=50, default=uuid.uuid4, editable=False, unique=True)
    AmCode = models.FileField(null=True, blank=True,)
