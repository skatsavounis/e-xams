from django.shortcuts import render

from rest_framework import viewsets
from TestersApp.models import AM
from TestersApp.serializers import AMSerializer

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny , IsAdminUser , IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import APIException
import uuid, os

class AmViewSet(viewsets.ModelViewSet):

    queryset = AM.objects.all() 
    serializer_class = AMSerializer 
    
    

    @action(detail=False , methods=['post'] , permission_classes=[AllowAny])
    def login(self , request):
        try:
            user = AM.objects.get(TestersAM=request.data['AM'] , unique_id=request.data['unique_id'])
            if not user.examtoken:
                user.examtoken = uuid.uuid4() 
                user.save()
        except AM.DoesNotExist:
            raise APIException('ti le re den yparxei')

        return Response( { user.examtoken } )

    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def check(self , request):
        try:
            AM.objects.get(examtoken=request.data['token'])
        except AM.DoesNotExist:
            return Response( False )
        
        return Response( True )

    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def code(self , request):
        try:
            thecode = AM.objects.get(examtoken=request.data['temptoken'])
            if os.path.exists(os.path.join(settings.MEDIA_ROOT, thecode.TestersAM)):
                os.remove(os.path.join(settings.MEDIA_ROOT, thecode.TestersAM))
            thecode.AmCode.save(thecode.TestersAM,ContentFile(request.data['code']))
        except AM.DoesNotExist:
            raise APIException('den yparxei')  
        
        return Response ( { request.data['code'] } )
    
    
     
