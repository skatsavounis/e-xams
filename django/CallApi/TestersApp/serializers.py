from rest_framework import serializers
from TestersApp.models import AM



class AMSerializer(serializers.ModelSerializer):
    class Meta:
        model = AM
        fields = "__all__"
