import logging
# Local Library
from threading import local
from rest_framework.response import Response
#_locals = local()


class Correlation():

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.META['UserToken'] = request.headers.get('Authorization')
     #   _local.examtoken = request.META["UserToken"]
        response = self.get_response(request)
        return response


#class CorrelationFilter(logging.Filter):
#
#    def filter(self, record):
#        if not hasattr(record, 'UserToken'):
#            record.UserToken = ""
#        if hasattr(_locals, 'UserToken'):
#            record.UserToken = _locals.UserToken
#        return True
